variable "ami" {
  type        = string
  description = "Ubuntu AMI ID in eu-central-1 Region"
  default     = "ami-079db87dc4c10ac91"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "My EC2 Instance"
}

variable "created_by" {
  type        = string
  description = "This vm created by GitLab-cicd"
  default     = "GitLab-cicd"
}
